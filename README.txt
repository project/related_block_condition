RELATED BLOCK VISIBILITY
========================

This modules provide a method to set the visibility of blocks by an entity
field.

For example you can place a block on a theme region and you can set the visibility for
nodes. In a node you can enable this block or not. If it is enabled the
visibility condition is validated and the block shows.

This module enable you an interactive method to show or not blocks based on
content.

STEPS TO CONFIGURE
--------------------

1. Enable the module.
2. Place the block in a region on the default theme.
3. Add a field to an entity the type of "Related block condition"
4. Edit/create an with this field and select the block you want to view.

NOTES:
-------

The module is based on plugins that can detect the entity that the user is
displaying. By default only four plugins was created for:

- Group
- Node
- Term
- User

If you have your custom entity you can create your own plugin and extend the
functionality of this module with your custom code.

Thanks for downloading this module.

SDOS
