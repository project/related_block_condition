<?php

namespace Drupal\related_block_condition\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Related block condition provider item annotation object.
 *
 * @see \Drupal\related_block_condition\Plugin\RelatedBlockConditionProviderManager
 * @see plugin_api
 *
 * @Annotation
 */
class RelatedBlockConditionProvider extends Plugin {


  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * The priority of the plugin.
   *
   * @var int
   */
  public $priority;

}
