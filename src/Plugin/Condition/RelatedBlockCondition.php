<?php

namespace Drupal\related_block_condition\Plugin\Condition;

use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\related_block_condition\Plugin\RelatedBlockConditionProviderManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Uuid\UuidInterface;

/**
 * Provides a 'Related block condition' condition.
 *
 * @Condition(
 *   id = "related_block_condition",
 *   label = @Translation("Related block condition"),
 * )
 */
class RelatedBlockCondition extends ConditionPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The provider manager.
   *
   * @var Drupal\related_block_condition\Plugin\RelatedBlockConditionProviderManagerInterface
   */
  protected $providerManager;

  /**
   * The uuid service.
   *
   * @var Drupal\Component\Uuid\UuidInterface
   */
  protected $uuidService;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
    $configuration,
    $plugin_id,
    $plugin_definition,
    $container->get('plugin.manager.related_block_condition_provider'),
    $container->get('uuid')
    );
  }

  /**
   * Creates a new RelatedBlockCondition object.
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name. The special key 'context' may be used to
   *   initialize the defined contexts by setting it to an array of context
   *   values keyed by context names.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param Drupal\related_block_condition\Plugin\RelatedBlockConditionProviderManagerInterface $provider_manager
   *   The provider manager.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid_service
   *   The UUID service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    RelatedBlockConditionProviderManagerInterface $provider_manager,
    UuidInterface $uuid_service
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->providerManager = $provider_manager;
    $this->uuidService = $uuid_service;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    // This setting stores if the condition must be evaluated.
    $plugins = $this->providerManager->getDefinitions();
    $options = [];
    foreach ($plugins as $plugin_id => $plugin) {
      $options[$plugin_id] = $plugin['label'];
    }
    $form['check_related'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Check to evaluate the visibility based on the current entity view.'),
      '#default_value' => $this->configuration['check_related'],
      '#options' => $options,
    ];

    // We need to know the uuid of the object to check if it has visibility.
    $uuid = !empty($this->configuration['check_uuid']) ? $this->configuration['check_uuid'] : $this->uuidService->generate();
    $form['check_uuid'] = [
      '#type' => 'value',
      '#value' => $uuid,
    ];

    $form['check_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label to be shown to users in the entity field as option.'),
      '#default_value' => $this->configuration['check_label'],
      '#description' => $this->t('If this field is empty the block title will be used as option in the field.'),
    ];

    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['check_related'] = array_filter($form_state->getValue('check_related'));
    $this->configuration['check_uuid'] = !empty($this->configuration['check_related']) ? $form_state->getValue('check_uuid') : '';
    $this->configuration['check_label'] = !empty($this->configuration['check_related']) ? $form_state->getValue('check_label') : '';
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['check_related' => [], 'check_uuid' => '', 'check_label' => ''] + parent::defaultConfiguration();
  }

  /**
   * Evaluates the condition and returns TRUE or FALSE accordingly.
   *
   * @return bool
   *   TRUE if the condition has been met, FALSE otherwise.
   */
  public function evaluate() {
    $related = array_filter($this->configuration['check_related']);
    if (empty($related) && !$this->isNegated()) {
      return TRUE;
    }
    $return = 0;
    $plugins = $this->providerManager->getDefinitions();
    foreach ($plugins as $plugin_id => $plugin) {
      $provider = $this->providerManager->createInstance($plugin_id);
      if (
        in_array($plugin_id, $this->configuration['check_related']) &&
        $provider &&
        !empty($this->configuration['check_uuid'])
      ) {
        if ($provider->checkCondition($this->configuration['check_uuid'])) {
          $return++;
        }
      }
    }
    return $return ? TRUE : FALSE;
  }

  /**
   * Provides a human readable summary of the condition's configuration.
   */
  public function summary() {
    $status = ($this->configuration['check_related']) ? t('enabled') : t('disabled');
    return $this->t('The related block condition is @status.', ['@status' => $status]);
  }

}
