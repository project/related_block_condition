<?php

namespace Drupal\related_block_condition\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\AllowedTagsXssTrait;
use Drupal\Core\Field\FieldFilteredMarkup;

/**
 * Plugin implementation of the 'related_block_condition_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "related_block_condition_formatter",
 *   label = @Translation("Related block condition formatter"),
 *   field_types = {
 *     "related_block_condition"
 *   }
 * )
 */
class RelatedBlockConditionFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  use AllowedTagsXssTrait;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new TimestampFormatter.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Third party settings.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    $label,
    $view_mode,
    array $third_party_settings,
    EntityTypeManagerInterface $entityTypeManager
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    $blocks = $this->entityTypeManager->getStorage('block')->loadMultiple();
    $options = [];
    $type = $this->fieldDefinition->getTargetEntityTypeId();
    foreach ($blocks as $block) {
      $visibility = $block->getVisibility();
      if (!empty($visibility) && !empty($visibility['related_block_condition']['check_related']['related_block_condition_provider_' . $type])) {
        $options[$visibility['related_block_condition']['check_uuid']] = !empty($visibility['related_block_condition']['check_label']) ? $visibility['related_block_condition']['check_label'] : $block->label();
      }
    }

    foreach ($items as $delta => $item) {
      if (isset($options[$item->value])) {
        $elements[$delta] = [
          '#markup' => $options[$item->value],
          '#allowed_tags' => FieldFilteredMarkup::allowedTags(),
        ];
      }
    }

    return $elements;
  }

}
