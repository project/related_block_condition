<?php

namespace Drupal\related_block_condition\Plugin\Field\FieldType;

use Drupal\Core\Field\Plugin\Field\FieldType\StringItem;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'related_block_condition' field type.
 *
 * @FieldType(
 *   id = "related_block_condition",
 *   label = @Translation("Related block condition"),
 *   description = @Translation("Save references to existents blocks to enable the visibility condition of them."),
 *   default_widget = "related_block_condition_widget",
 *   default_formatter = "related_block_condition_formatter"
 * )
 */
class RelatedBlockCondition extends StringItem {

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'max_length' => 128,
      'is_ascii' => TRUE,
    ] + parent::defaultStorageSettings();
  }

}
