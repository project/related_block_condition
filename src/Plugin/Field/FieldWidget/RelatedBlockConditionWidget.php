<?php

namespace Drupal\related_block_condition\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsButtonsWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Plugin implementation of the 'related_block_condition_widget' widget.
 *
 * @FieldWidget(
 *   id = "related_block_condition_widget",
 *   label = @Translation("Related block condition widget"),
 *   field_types = {
 *     "related_block_condition"
 *   },
 *   multiple_values = TRUE
 * )
 */
class RelatedBlockConditionWidget extends OptionsButtonsWidget implements ContainerFactoryPluginInterface {

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    array $third_party_settings,
    EntityTypeManagerInterface $entityTypeManager
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $blocks = $this->entityTypeManager->getStorage('block')->loadMultiple();
    $options = [];
    $type = $this->fieldDefinition->getTargetEntityTypeId();
    foreach ($blocks as $block) {
      $visibility = $block->getVisibility();
      if (!empty($visibility) && !empty($visibility['related_block_condition']['check_related']['related_block_condition_provider_' . $type])) {
        $options[$visibility['related_block_condition']['check_uuid']] = !empty($visibility['related_block_condition']['check_label']) ? $visibility['related_block_condition']['check_label'] : $block->label();
      }
    }
    $this->options = $options;
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $selected = $this->getSelectedOptions($items);

    // If required and there is one single option, preselect it.
    if ($this->required && count($options) == 1) {
      reset($options);
      $selected = [key($options)];
    }

    if ($this->multiple) {
      $element += [
        '#type' => 'checkboxes',
        '#default_value' => $selected,
        '#options' => $options,
      ];
    }
    else {
      $element += [
        '#type' => 'radios',
        '#default_value' => $selected ? reset($selected) : NULL,
        '#options' => $options,
      ];
    }

    return $element;

  }

}
