<?php

namespace Drupal\related_block_condition\Plugin\RelatedBlockConditionProvider;

use Drupal\related_block_condition\Plugin\RelatedBlockConditionProviderBase;
use Drupal\Core\Entity\EntityInterface;

// The id must end with the entiy type machine name.
/**
 * Provides integration with Group.
 *
 * @RelatedBlockConditionProvider(
 *  id = "related_block_condition_provider_group",
 *  label = @Translation("The group provider."),
 *  priority = 2,
 * )
 */
class RelatedBlockConditionProviderGroup extends RelatedBlockConditionProviderBase {

  /**
   * {@inheritdoc}
   */
  public function checkCondition(string $condition_uuid) {
    $entity = $this->routeMatch->getParameter('group');
    if ($entity && $entity instanceof EntityInterface) {
      return $this->checkEntityCondition($entity, $condition_uuid);
    }
    return FALSE;
  }

}
