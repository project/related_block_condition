<?php

namespace Drupal\related_block_condition\Plugin\RelatedBlockConditionProvider;

use Drupal\related_block_condition\Plugin\RelatedBlockConditionProviderBase;
use Drupal\Core\Entity\EntityInterface;

// The id must end with the entiy type machine name.
/**
 * Provides integration with Users.
 *
 * @RelatedBlockConditionProvider(
 *  id = "related_block_condition_provider_user",
 *  label = @Translation("The user provider."),
 *  priority = 0,
 * )
 */
class RelatedBlockConditionProviderUser extends RelatedBlockConditionProviderBase {

  /**
   * {@inheritdoc}
   */
  public function checkCondition(string $condition_uuid) {
    $entity = $this->routeMatch->getParameter('user');
    if ($entity && $entity instanceof EntityInterface) {
      return $this->checkEntityCondition($entity, $condition_uuid);
    }
    return FALSE;
  }

}
