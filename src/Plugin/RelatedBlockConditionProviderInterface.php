<?php

namespace Drupal\related_block_condition\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Related block condition provider plugins.
 */
interface RelatedBlockConditionProviderInterface extends PluginInspectionInterface {

  /**
   * Get the label of the plugin.
   *
   * @return string
   *   The label.
   */
  public function label();

  /**
   * Get the priority of the plugin.
   *
   * @return int
   *   The priority.
   */
  public function priority();

  /**
   * Check the visibility of the block based on current provider.
   *
   * @param string $condition_uuid
   *   The uuid of the condition in the block to check.
   *
   * @return bool
   *   Return if the block must show or not.
   */
  public function checkCondition(string $condition_uuid);

}
