<?php

namespace Drupal\related_block_condition\Plugin;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides the Related block condition provider plugin manager.
 */
class RelatedBlockConditionProviderManager extends DefaultPluginManager implements RelatedBlockConditionProviderManagerInterface {

  /**
   * Constructs a new RelatedBlockConditionProviderManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/RelatedBlockConditionProvider', $namespaces, $module_handler, 'Drupal\related_block_condition\Plugin\RelatedBlockConditionProviderInterface', 'Drupal\related_block_condition\Annotation\RelatedBlockConditionProvider');

    $this->alterInfo('related_block_condition_related_block_condition_provider_info');
    $this->setCacheBackend($cache_backend, 'related_block_condition_related_block_condition_provider_plugins');
  }

  /**
   * Find definitions with priority control.
   *
   * @return array
   *   List of definitions to store in cache.
   */
  protected function findDefinitions() {
    $definitions = parent::findDefinitions();
    uasort($definitions, [$this, 'sortByPriority']);
    return $definitions;
  }

  /**
   * Function to evaluate the priority of the definitions.
   *
   * @param array $a
   *   The first param of the comparison.
   * @param array $b
   *   The second param of the comparison.
   *
   * @return int
   *   The difference of the comparison.
   */
  protected function sortByPriority(array $a, array $b) {
    return $a['priority'] - $b['priority'];
  }

}
