<?php

namespace Drupal\related_block_condition\Plugin;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Provides the Related block condition provider plugin manager.
 */
interface RelatedBlockConditionProviderManagerInterface extends PluginManagerInterface {

}
